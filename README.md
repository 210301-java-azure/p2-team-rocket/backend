# Team Rocket Tryouts
## Project Description
Team Rocket is recruiting! Attempt our tryouts to see if you have what it takes to be a Team Rocket Grunt.
http://teamrockettryouts.z5.web.core.windows.net/
## Technologies Used
- Angular 11
- PokeAPI
- Spring Boot
- Spring Data
- Spring Web
- Spring Security
- Hibernate ORM
- Jackson JSON Processor
## Features
- Register account and set avatar.
- Choose your pokemon and an opponent for a battle.
- Have your pokemon battle against the opponent's pokemon to pass the Team Rocket Tryouts.<br>
*To-do List:*
- Select and battle with multiple Pokemon.
- Fully implement ranking system.
- Animation system.
## Getting Started
Clone these for installation.
<br>
<ins>Front-End Clone</ins><br>
https://gitlab.com/210301-java-azure/p2-team-rocket/frontend.git

<ins>Back-End Clone</ins><br>
https://gitlab.com/210301-java-azure/p2-team-rocket/backend.git
## Usage
1) Register an account.<br>
2) While logged-in, click on the Battle Station in the nav-bar.
3) Bring up the pokemon selection screen by clicking on the avatar on the left-hand side. From here, select the Pokemon you would like to use in the upcoming battle by clicking on their picture.
4) Bring up the opponent selection screen by clicking on the avatar on the right-hand side. From here, select the opponent you would like to battle against by clicking on their name. 
5) Click on the *Ready* button once you're prepared to battle. 
6) The player gets the first move. Click on the *fight* button and select which move you would like your Pokemon to perform. After a couple of seconds, your opponent's Pokemon will automatically fight back with a move of its own. The *Run* button will immediately return you to the Home page.
7) If you beat your opponent, then congratulations on joining Team Rocket! Otherwise practice some more and try again.
## Contributors
Steve - Team Lead<br>
Austin - Scrum Master<br>
Cody - Git Maintainer
## License
Team Rocket Tryouts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Team Rocket Tryouts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Team Rocket Tryouts.  If not, see https://www.gnu.org/licenses/ .