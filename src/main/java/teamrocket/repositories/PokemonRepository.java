package teamrocket.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import teamrocket.models.Pokemon;

@Repository
public interface PokemonRepository extends JpaRepository<Pokemon, Integer> {


}
