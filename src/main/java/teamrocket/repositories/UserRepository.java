package teamrocket.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import teamrocket.models.UserAccount;

@Repository
    public interface UserRepository extends JpaRepository<UserAccount, Integer> {
        public UserAccount findByUsername(String username);
        public UserAccount findById(int id);
}