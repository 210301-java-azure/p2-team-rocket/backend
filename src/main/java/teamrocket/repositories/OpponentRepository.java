package teamrocket.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import teamrocket.models.Opponent;

@Repository
public interface OpponentRepository extends JpaRepository<Opponent,Integer> {   }
