package teamrocket.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Scope("prototype")
@JsonIgnoreProperties("hibernateLazyInitializer")
@Component
public class Opponent implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "Opponent_Pokemon",
            joinColumns = {@JoinColumn(name = "opp_id")},
            inverseJoinColumns = {@JoinColumn(name = "poke_id")}
    )
    private Set<Pokemon> pokemonSet= new HashSet<>();

    public Opponent() { }

    public Opponent(int id, String name, Set<Pokemon> pokemonSet) {
        this.id = id;
        this.name = name;
        this.pokemonSet = pokemonSet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Pokemon> getPokemonSet() {
        return pokemonSet;
    }

    public void setPokemonSet(Set<Pokemon> pokemonSet) {
        this.pokemonSet = pokemonSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Opponent opponent = (Opponent) o;
        return id == opponent.id && Objects.equals(name, opponent.name) && Objects.equals(pokemonSet, opponent.pokemonSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, pokemonSet);
    }

    @Override
    public String toString() {
        return "Opponent{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pokemonSet=" + pokemonSet +
                '}';
    }
}
