package teamrocket.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Scope("prototype")
@JsonIgnoreProperties("hibernateLazyInitializer")
@Component
public class Pokemon implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int API_id;
    private int Move_1;
    private int Move_2;
    private int Move_3;
    private int Move_4;

    @Transient
    @ManyToMany(mappedBy="pokemonSet", fetch = FetchType.EAGER)
    private Set<Opponent> opponentSet = new HashSet<>();

    public Pokemon() { }

    public Pokemon(int id, int API_id, int move_1, int move_2, int move_3, int move_4, Set<Opponent> opponentSet) {
        this.id = id;
        this.API_id = API_id;
        Move_1 = move_1;
        Move_2 = move_2;
        Move_3 = move_3;
        Move_4 = move_4;
        this.opponentSet = opponentSet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAPI_id() {
        return API_id;
    }

    public void setAPI_id(int API_id) {
        this.API_id = API_id;
    }

    public int getMove_1() {
        return Move_1;
    }

    public void setMove_1(int move_1) {
        Move_1 = move_1;
    }

    public int getMove_2() {
        return Move_2;
    }

    public void setMove_2(int move_2) {
        Move_2 = move_2;
    }

    public int getMove_3() {
        return Move_3;
    }

    public void setMove_3(int move_3) {
        Move_3 = move_3;
    }

    public int getMove_4() {
        return Move_4;
    }

    public void setMove_4(int move_4) {
        Move_4 = move_4;
    }

    public Set<Opponent> getOpponentSet() { return opponentSet;}

    public void setOpponentSet(Set<Opponent> opponentSet) {
        this.opponentSet = opponentSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pokemon pokemon = (Pokemon) o;
        return id == pokemon.id && API_id == pokemon.API_id && Move_1 == pokemon.Move_1 && Move_2 == pokemon.Move_2 && Move_3 == pokemon.Move_3 && Move_4 == pokemon.Move_4 && Objects.equals(opponentSet, pokemon.opponentSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, API_id, Move_1, Move_2, Move_3, Move_4, opponentSet);
    }

    @Override
    public String toString() {
        return "Pokemon{" +
                "id=" + id +
                ", API_id=" + API_id +
                ", Move_1=" + Move_1 +
                ", Move_2=" + Move_2 +
                ", Move_3=" + Move_3 +
                ", Move_4=" + Move_4 +
                ", opponentSet=" + opponentSet +
                '}';
    }
}
