package teamrocket.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

import java.time.LocalDate;
import java.util.Objects;

@Entity
@Scope("prototype")
@JsonIgnoreProperties("hibernateLazyInitializer")
@Component
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "email",columnNames = {"email" }),
        @UniqueConstraint(name= "username",columnNames={"username"})
        })
public class UserAccount implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String username;
    private String password;
    private String email;
    private Integer rank;
    @CreationTimestamp
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate registerDate;
    private Integer avatar;

    public UserAccount() { }

    public UserAccount(int id, String username, String password, String email, Integer rank, Integer avatar) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.rank = rank;
        this.registerDate = LocalDate.now();
        this.avatar = avatar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public LocalDate getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(LocalDate registerDate) {
        this.registerDate = registerDate;
    }

    public Integer getAvatar() {
        return avatar;
    }

    public void setAvatar(Integer avatar) {
        this.avatar = avatar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAccount that = (UserAccount) o;
        return id == that.id && Objects.equals(username, that.username) && Objects.equals(password, that.password) && Objects.equals(email, that.email) && Objects.equals(rank, that.rank) && Objects.equals(registerDate, that.registerDate) && Objects.equals(avatar, that.avatar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, email, rank, registerDate, avatar);
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", rank=" + rank +
                ", registerDate=" + registerDate +
                ", avatar=" + avatar +
                '}';
    }
}