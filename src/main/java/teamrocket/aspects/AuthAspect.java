package teamrocket.aspects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Component
@Aspect
public class AuthAspect {

    private final Logger logger=LogManager.getLogger(AuthAspect.class);

    @Around("within(teamrocket.controllers.*)"+"&&!this(teamrocket.controllers.RegisterUserController)")
    public ResponseEntity<ProceedingJoinPoint> authorizeRequest(ProceedingJoinPoint jp) throws Throwable {
        HttpServletRequest request=
                ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String token=request.getHeader("Authorization");
        if(token!=null){
            return (ResponseEntity) jp.proceed();
        }else{
            logger.warn("auth header not present");
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }
}
