package teamrocket.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import teamrocket.models.Pokemon;
import teamrocket.services.PokemonService;


import java.util.List;
@RestController
@RequestMapping("/pokemon")
@CrossOrigin
public class PokemonController {

    @Autowired
    PokemonService pokemonService;

    @GetMapping(value = "/{id}",produces = "application/json")
    public ResponseEntity<Pokemon> getPokemonById(@PathVariable("id") int id) {
        return ResponseEntity.ok(pokemonService.getById(id));
    }
    @GetMapping
    public ResponseEntity<List<Pokemon>> getAllPokemon() {
        return ResponseEntity.ok().body(pokemonService.getAll());
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Pokemon> addPokemon(@RequestBody Pokemon pokemon){
        pokemonService.create(pokemon);
        return ResponseEntity.ok(pokemon);
    }
}
