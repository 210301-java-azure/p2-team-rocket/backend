package teamrocket.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import teamrocket.models.UserAccount;
import teamrocket.services.UserService;

import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping(path="/register")
@CrossOrigin
public class RegisterUserController {

    Logger LOGGER= LoggerFactory.getLogger(RegisterUserController.class);
    @Autowired
    private UserService userService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UserAccount> addNewUser (@RequestBody UserAccount userAccount) throws NoSuchAlgorithmException {
        userAccount.setAvatar(0);
        userAccount.setRank(1);

        LOGGER.info("Creating new user account");
        userService.create(userAccount);
        return ResponseEntity.ok(userAccount);
    }


}
