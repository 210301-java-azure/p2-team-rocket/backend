package teamrocket.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import teamrocket.models.Opponent;
import teamrocket.services.OpponentService;
import java.util.List;

@RestController
@RequestMapping(path = "/opponent")
@CrossOrigin
public class OpponentController {

    @Autowired
    OpponentService opponentService;


    @GetMapping()
    public ResponseEntity<List<Opponent>>getAllOpponents(){
        return ResponseEntity.ok().body(opponentService.getAll());
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<Opponent> createNewOpponent(@RequestBody Opponent opponent){
        opponentService.create(opponent);
        return new ResponseEntity<>(opponent, HttpStatus.CREATED);
    }
}
