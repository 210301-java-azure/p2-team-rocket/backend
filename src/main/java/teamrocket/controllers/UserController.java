package teamrocket.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import teamrocket.models.UserAccount;
import teamrocket.services.UserService;

@RestController
@RequestMapping(path = "/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/{username}")
    public ResponseEntity<UserAccount> getUserByName(@PathVariable String username) {

        return ResponseEntity.ok(userService.findUserByName(username));
    }

    @DeleteMapping("/{username}")
    public void deleteUserByName(@PathVariable String username) {
         userService.deleteUserByName(username);
    }

    @PatchMapping("/update")
    public ResponseEntity<UserAccount> partialUpdateName(
            @RequestBody UserAccount partialUpdate) {
            userService.updateUserByName(partialUpdate);
            return ResponseEntity.ok(partialUpdate);
    }

    @PostMapping("/login")
    public ResponseEntity<UserAccount> attemptLogin(@RequestBody UserAccount loginUser){
        BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
        UserAccount user = userService.findUserByName(loginUser.getUsername());
        if (user!=null){
            if (bc.matches(loginUser.getPassword(),user.getPassword())){
                return ResponseEntity.ok(user);
            }else{
                      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        }else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}