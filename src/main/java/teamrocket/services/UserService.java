package teamrocket.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
//import org.springframework.s
import teamrocket.models.UserAccount;
import teamrocket.repositories.UserRepository;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    //create new user.
    public void create(UserAccount userAccount){
        BCryptPasswordEncoder bc = new BCryptPasswordEncoder(5);
        userAccount.setPassword(bc.encode(userAccount.getPassword()));
        userRepository.save(userAccount);
    }
    //reads user by username.
    public UserAccount findUserByName(String username) {
        return userRepository.findByUsername(username);
    }
    //delete user by username
    public void deleteUserByName(String username) {
        UserAccount userAccount;
        userAccount = findUserByName(username);
        userRepository.delete(userAccount);
    }
    //update user by id
    public void updateUserByName(UserAccount partialUpdate) {
        UserAccount account = userRepository.findById(partialUpdate.getId());
        account.setEmail(partialUpdate.getEmail());
        account.setAvatar(partialUpdate.getAvatar());
        account.setUsername(partialUpdate.getUsername());
        userRepository.save(account);
    }
}