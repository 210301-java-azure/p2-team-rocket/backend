package teamrocket.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import teamrocket.models.Pokemon;
import teamrocket.repositories.PokemonRepository;

import java.util.List;

@Service
public class PokemonService {

    @Autowired
    PokemonRepository pokemonRepository;

    public List<Pokemon> getAll(){ return pokemonRepository.findAll();}
    public Pokemon getById(int id){ return pokemonRepository.getOne(id);}
    public void create(Pokemon pokemon){ pokemonRepository.save(pokemon); }
}

