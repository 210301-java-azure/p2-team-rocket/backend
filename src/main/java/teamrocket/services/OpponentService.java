package teamrocket.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import teamrocket.models.Opponent;
import teamrocket.repositories.OpponentRepository;

import java.util.List;

@Service
public class OpponentService {

    @Autowired
    OpponentRepository opponentRepository;

    public List<Opponent> getAll(){
        return opponentRepository.findAll();
    }
    public void create(Opponent opponent){
        opponentRepository.save(opponent);
    }


}
