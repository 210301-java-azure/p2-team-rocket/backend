FROM java:8
COPY build/libs/Team-Rocket-Server-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar Team-Rocket-Server-1.0-SNAPSHOT.jar